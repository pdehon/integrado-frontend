import { useState, useEffect } from "react";
import AsyncStorage from "@react-native-async-storage/async-storage";

type StorageValue<T> = T | null;

function useAsyncStorage<T>(key: string) {
  const [storageValue, setStorageStateValue] = useState<StorageValue<T>>(null);
  const [storageValueLoaded, setStorageValueLoaded] = useState(false);

  useEffect(() => {
    async function fetchStorageValue() {
      try {
        const value = await AsyncStorage.getItem(key);
        if (value !== null) {
          setStorageStateValue(JSON.parse(value) as T);
        }
      } catch (error) {
        Promise.reject(error);
      } finally {
        setStorageValueLoaded(true);
      }
    }

    fetchStorageValue();
  }, [key]);

  const setStorageValue = async (value: T) => {
    try {
      const serializedValue = JSON.stringify(value);
      await AsyncStorage.setItem(key, serializedValue);
      setStorageValue(value);
    } catch (error) {
      Promise.reject(error);
    }
  };

  return {
    storageValue,
    setStorageValue,
    storageValueLoaded,
  };
}

export default useAsyncStorage;
