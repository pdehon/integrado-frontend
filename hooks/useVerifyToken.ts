import { useEffect, useState } from "react";
import useAsyncStorage from "./useStorage";

const TOKEN_KEY = "@encore/token";

const useVerifyToken = () => {
  const [isUserAuthenticated, setIsUserAuthenticated] = useState(false);
  const { storageValue: token, storageValueLoaded } =
    useAsyncStorage<string>(TOKEN_KEY);

  useEffect(() => {
    if (storageValueLoaded) setIsUserAuthenticated(!!token);
  }, [storageValueLoaded]);

  return {
    isUserAuthenticated,
    tokenLoaded: storageValueLoaded,
  };
};

export default useVerifyToken;
