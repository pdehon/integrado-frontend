import { useState } from "react";

import userApi from "@modules/userApi";
import useAsyncStorage from "./useStorage";
import { useAppDispatch } from "./redux";
import { setUserData } from "@redux/user";

const TOKEN_KEY = "@encore/token";

const useAuth = () => {
  const [isLoading, setIsLoading] = useState(false);
  const { setStorageValue } = useAsyncStorage(TOKEN_KEY);
  const dispatch = useAppDispatch();

  const signin = async (
    email: string,
    password: string,
    callback: () => void
  ) => {
    setIsLoading(true);
    const response: any = await userApi.signin(email, password);
    setStorageValue(response.token);
    dispatch(
      setUserData({
        name: "Usuário 1",
        token: response.token,
      })
    );
    setIsLoading(false);
    callback();
  };

  const logout = async (callback: () => void) => {
    setIsLoading(true);
    setStorageValue("");
    dispatch(
      setUserData({
        name: "Usuário 1",
        token: "",
      })
    );
    setTimeout(() => {
      callback();
      setIsLoading(false);
    }, 1000);
  };

  return {
    isLoading,
    signin,
    logout,
  };
};

export default useAuth;
