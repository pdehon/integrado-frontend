import { StyleSheet } from "react-native";

export const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: "center",
    padding: 20,
  },
  title: {
    width: "100%",
    textAlign: "left",
    fontSize: 18,
    fontFamily: "Roboto_500Medium",
    fontWeight: "500",
    marginBottom: 20,
  },
  mailInput: {
    marginBottom: 16,
  },
  continueButton: {
    marginTop: 24,
  },
  hasNoConnectionText: {
    fontSize: 14,
    fontFamily: "Roboto_400Regular",
    fontWeight: "400",
    paddingVertical: 14,
  },
  link: {
    color: "#5ea4de",
    textDecorationLine: "underline",
    fontFamily: "Roboto_500Medium",
    fontWeight: "500",
  },
});
