import { View, Text, TouchableOpacity, Keyboard } from "react-native";
import { SafeAreaView } from "react-native-safe-area-context";
import { Image } from "expo-image";

import TextInput from "@components/text-input";
import Button from "@components/button";
import DismissKeyboardView from "@components/dismiss-keyboard-view";

import { styles } from "./style";
import { useState } from "react";
import useAuth from "@hooks/useAuth";
import { useRouter } from "expo-router";

function Signin() {
  const [mail, setMail] = useState("");
  const [password, setPassword] = useState("");
  const router = useRouter();

  const { isLoading, signin } = useAuth();

  const login = () => {
    signin(mail, password, () => {
      Keyboard.dismiss();
      setTimeout(() => {
        router.push("/");
      }, 300);
    });
  };

  return (
    <SafeAreaView style={{ flex: 1 }}>
      <DismissKeyboardView style={styles.container}>
        <Image
          style={{ width: 150, height: 150 }}
          source={require("@images/encore-logo.png")}
        />
        <Text style={styles.title}>Login</Text>
        <TextInput
          label="Email"
          style={styles.mailInput}
          onChangeText={(text) => setMail(text)}
        />
        <TextInput
          label="Senha"
          textContentType="password"
          onChangeText={(text) => setPassword(text)}
        />
        <Button
          label="Continuar"
          style={styles.continueButton}
          onPress={login}
          isLoading={isLoading}
          disabled={!mail || !password}
        />
        <Text style={styles.hasNoConnectionText}>
          Está sem conexão? <Text style={styles.link}>Usar offline</Text>
        </Text>
      </DismissKeyboardView>
    </SafeAreaView>
  );
}

export default Signin;
