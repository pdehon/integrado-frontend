import { StyleSheet, Text, View } from "react-native";
import { Link, Stack, useRouter } from "expo-router";

import Header from "@components/header";
import { useAppSelector } from "@hooks/redux";
import Button from "@components/button";
import useAuth from "@hooks/useAuth";

export default function Page() {
  const { name } = useAppSelector((state) => state.user);
  const { isLoading, logout } = useAuth();
  const router = useRouter();

  return (
    <View style={styles.container}>
      <View style={styles.main}>
        <Header />
        <Text style={styles.title}>Ola {name}</Text>
        <Text style={styles.subtitle}>This is the first page of your app.</Text>
        <Button
          label="Logout"
          isLoading={isLoading}
          onPress={() => {
            logout(() => {
              router.push("/signin");
            });
          }}
        />
      </View>
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: "center",
    justifyContent: "center",
    padding: 24,
  },
  main: {
    width: "100%",
    justifyContent: "center",
    marginHorizontal: "auto",
  },
  title: {
    fontSize: 48,
    fontFamily: "Roboto_500Medium",
  },
  subtitle: {
    fontSize: 36,
    color: "#38434D",
    marginBottom: 16,
  },
});
