import { useEffect } from "react";
import { SplashScreen, Slot, useRouter } from "expo-router";
import { Provider } from "react-redux";

import {
  useFonts,
  Roboto_400Regular,
  Roboto_500Medium,
  Roboto_700Bold,
} from "@expo-google-fonts/roboto";
import { store } from "@redux/store";
import useVerifyToken from "@hooks/useVerifyToken";

export default function Layout() {
  const router = useRouter();

  const { isUserAuthenticated, tokenLoaded } = useVerifyToken();

  const [fontsLoaded] = useFonts({
    Roboto_400Regular,
    Roboto_500Medium,
    Roboto_700Bold,
  });

  useEffect(() => {
    if (fontsLoaded && tokenLoaded) {
      if (isUserAuthenticated) {
        router.push("/");
        return;
      }
      router.push("/signin");
    }
  }, [fontsLoaded, tokenLoaded, isUserAuthenticated]);

  if (!fontsLoaded || !tokenLoaded) {
    // Render the splash screen while we load the assets.
    return <SplashScreen />;
  }

  // Render the children routes now that all the assets are loaded.
  return (
    <Provider store={store}>
      <Slot />
    </Provider>
  );
}
