import api from "./api";

export const getUser = (id: string) => {
  return api.get(`/users/${id}`);
};

export const signin = (email: string, password: string) => {
  const fakeToken = "kdahlkwdjalkwdjjadj2k1e12dawdawd";
  return new Promise((resolve, reject) => {
    setTimeout(() => {
      resolve({
        token: fakeToken,
        name: "Usuário 1",
      });
    }, 1000);
  });
};

export default {
  getUser,
  signin,
};
