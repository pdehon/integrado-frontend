import {
  Text,
  TouchableOpacity,
  TouchableOpacityProps,
  ActivityIndicator,
} from "react-native";

import { styles } from "./style";

interface ButtonProps extends TouchableOpacityProps {
  label?: string;
  type?: "primary" | "secondary" | "link";
  isLoading?: boolean;
}

function Button({
  label,
  type = "primary",
  isLoading,
  style,
  disabled,
  ...rest
}: ButtonProps) {
  return (
    <TouchableOpacity
      {...rest}
      disabled={disabled || isLoading}
      style={[
        styles.container,
        styles[`${type}-container`],
        disabled && styles.disabled,
        style,
      ]}
    >
      {isLoading ? (
        <ActivityIndicator
          size="small"
          color={type === "primary" ? "#FFF" : "#5ea4de"}
        />
      ) : (
        <Text style={[styles.label, styles[`${type}-label`]]}>{label}</Text>
      )}
    </TouchableOpacity>
  );
}

export default Button;
