import { StyleSheet } from "react-native";

export const styles = StyleSheet.create({
  container: {
    width: "100%",
    borderRadius: 45,
    paddingVertical: 16,
    paddingHorizontal: 12,
    alignItems: "center",
  },
  "primary-container": {
    backgroundColor: "#5ea4de",
  },
  "secondary-container": {
    backgroundColor: "#FFF",
    borderWidth: 1,
    borderColor: "#5ea4de",
  },
  "link-container": {},
  label: {
    fontSize: 16,
    fontFamily: "Roboto_500Medium",
    fontWeight: "500",
  },
  "primary-label": {
    color: "#FFF",
  },
  "secondary-label": {
    color: "#5ea4de",
  },
  "link-label": {
    color: "#5ea4de",
  },
  disabled: {
    opacity: 0.5,
  },
});
