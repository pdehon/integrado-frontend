import {
  Keyboard,
  TouchableWithoutFeedback,
  ViewStyle,
  StyleProp,
  View,
} from "react-native";

interface DismissKeyboardViewProps {
  children: React.ReactNode;
  style?: StyleProp<ViewStyle>;
}

function DismissKeyboardView({ children, style }: DismissKeyboardViewProps) {
  return (
    <TouchableWithoutFeedback onPress={Keyboard.dismiss}>
      <View style={style}>{children}</View>
    </TouchableWithoutFeedback>
  );
}

export default DismissKeyboardView;
