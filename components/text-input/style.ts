import { StyleSheet } from "react-native";

export const styles = StyleSheet.create({
  container: {
    flexDirection: "row",
    justifyContent: "center",
    alignItems: "flex-end",
    width: "100%",
    height: 56,
    backgroundColor: "#f1f1f1",
    borderRadius: 16,
    paddingLeft: 16,
  },
  label: {
    position: "absolute",
    left: 16,
    top: 15,
    fontFamily: "Roboto_500Medium",
  },
  input: {
    flex: 1,
    width: "100%",
    height: 56,
    fontSize: 16,
    fontFamily: "Roboto_500Medium",
    color: "#202020",
  },
  iconContainer: {
    height: 56,
    justifyContent: "center",
    alignItems: "center",
    paddingLeft: 4,
    paddingRight: 12,
  },
});
