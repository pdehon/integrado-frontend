import { useEffect } from "react";
import {
  useAnimatedStyle,
  useSharedValue,
  withTiming,
} from "react-native-reanimated";

const initialAnimationState = {
  translateY: 0,
  opacity: 0,
  fontSize: 16,
};

const useAnimation = (value: string, focused: boolean) => {
  const offset = useSharedValue({ ...initialAnimationState });

  useEffect(() => {
    if (value || focused) {
      offset.value = {
        translateY: -15,
        opacity: 1,
        fontSize: 12,
      };
    } else {
      offset.value = { ...initialAnimationState };
    }
  }, [value, focused]);

  const animatedStyles = useAnimatedStyle(() => {
    return {
      transform: [{ translateY: withTiming(offset.value.translateY) }],
      opacity: withTiming(offset.value.opacity),
      fontSize: withTiming(offset.value.fontSize),
    };
  });

  return { offset, animatedStyles };
};

export default useAnimation;
