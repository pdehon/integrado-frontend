import { useState } from "react";
import {
  View,
  TextInput as TextInputRN,
  TextInputProps as TextInputRNProps,
  TouchableOpacity,
  NativeSyntheticEvent,
  TextInputFocusEventData,
} from "react-native";
import Animated from "react-native-reanimated";
import Ionicons from "@expo/vector-icons/MaterialCommunityIcons";

import { styles } from "./style";
import useAnimation from "./animation";

interface TextInputProps extends TextInputRNProps {
  label?: string;
  labelColor?: string;
}

function TextInput({
  label,
  placeholder,
  onFocus,
  onChangeText,
  onBlur,
  textContentType,
  style,
  labelColor = "#5ea4de",
  ...rest
}: TextInputProps) {
  const [focused, setFocused] = useState(false);
  const [value, setValue] = useState("");
  const [isPasswordVisible, setIsPasswordVisible] = useState(false);

  const { animatedStyles } = useAnimation(value, focused);

  const onFocusOverwrite = (
    e: NativeSyntheticEvent<TextInputFocusEventData>
  ) => {
    setFocused(true);
    if (onFocus) onFocus(e);
  };

  const onBlurOverwrite = (
    e: NativeSyntheticEvent<TextInputFocusEventData>
  ) => {
    setFocused(false);
    if (onBlur) onBlur(e);
  };

  const getPlaceholder = () => {
    if (focused && placeholder) return placeholder;

    return label;
  };

  const Label = () => {
    if (label)
      return (
        <Animated.Text
          style={[styles.label, { color: labelColor }, animatedStyles]}
        >
          {label}
        </Animated.Text>
      );

    return null;
  };

  const HidePasswordICon = () => {
    if (textContentType === "password" && value)
      return (
        <TouchableOpacity
          style={styles.iconContainer}
          onPress={() => setIsPasswordVisible((prevState) => !prevState)}
        >
          <Ionicons
            name={isPasswordVisible ? "eye-off" : "eye"}
            size={18}
            color="black"
          />
        </TouchableOpacity>
      );
    return null;
  };

  return (
    <View style={[styles.container, style]}>
      <Label />
      <TextInputRN
        placeholder={getPlaceholder()}
        style={styles.input}
        onFocus={onFocusOverwrite}
        onBlur={onBlurOverwrite}
        onChangeText={(text) => {
          setValue(text);
          if (onChangeText) onChangeText(text);
        }}
        secureTextEntry={textContentType === "password" && !isPasswordVisible}
        {...rest}
      />
      <HidePasswordICon />
    </View>
  );
}

export default TextInput;
