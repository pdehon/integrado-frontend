module.exports = function (api) {
  api.cache(true);
  return {
    presets: ["babel-preset-expo"],
    plugins: [
      require.resolve("expo-router/babel"),
      "react-native-reanimated/plugin",
      [
        "module-resolver",
        {
          alias: {
            "@components": "./components",
            "@modules": "./modules",
            "@utils": "./utils",
            "@redux": "./redux",
            "@hooks": "./hooks",
            "@images": "./images",
          },
        },
      ],
    ],
  };
};
