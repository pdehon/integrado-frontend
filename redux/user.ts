import { createSlice, PayloadAction } from "@reduxjs/toolkit";

export interface UserState {
  token: string;
  name: string;
}

const user = createSlice({
  name: "user",
  initialState: {
    name: "Usuário 1",
    token: "",
  },
  reducers: {
    setUserData(state, action: PayloadAction<UserState>) {
      state = action.payload;
    },
  },
});

export const { setUserData } = user.actions;
export default user.reducer;
